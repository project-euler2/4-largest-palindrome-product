def largest_palindrom_product(dimension):
    largest_palindrom = 0
    for i in range(1,10**dimension):
        for j in range(1,10**dimension):
            number = i*j
            if i*j > largest_palindrom:
                number_str = str(number)
                if number_str == number_str[::-1]:
                    largest_palindrom = i*j

    return largest_palindrom

print(largest_palindrom_product(3))